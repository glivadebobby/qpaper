<?php

require_once('config.php');

if (isset($_POST['download-paper'])) {
	$id = $_POST['id'];

	$query = "SELECT exam_question.id, question.question, question.mark FROM exam_question LEFT JOIN question ON exam_question.question_id = question.id WHERE subject_id = '$id'";
	$result = mysqli_query($con, $query);

	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=question-paper.csv');

	$output = fopen('php://output', 'w');

	fputcsv($output, array('S.No.', 'Question', 'Mark'));

	$i = 1;
	while ($row = mysqli_fetch_assoc($result)) {
		$data = array();
		$data['s_no'] = $i;
		$data['question'] = $row['question'];
		$data['mark'] = $row['mark'];
		fputcsv($output, $data);
		$i++;
	} 
}

mysqli_close($con);

?>