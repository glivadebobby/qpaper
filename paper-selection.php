<?php

require_once('common.php');

$title = "Paper Selection";

include_once('header.php');

include_once('sidebar.php');
?>

<?php

$success = "";
$error = "";

if (isset($_POST['add-data'])) {
	$subject = $_POST['add-subject'];
	$question_sets = $_POST['add-question-set'];

	for ($i = 0; $i < count($question_sets); $i++) {
		$question_set = $question_sets[$i];
		$query1 = "INSERT INTO selected_question(subject_id, question_set_id) VALUES('$subject', '$question_set')";
		$result1 = mysqli_query($con, $query1);

		if($result1) {
			$success = "Question paper selected!";
		} else {
			$error = "Question paper already exists!";
		}
	}
}

if (isset($_POST['delete-data'])) {
	$id = $_POST['delete-id'];

	$query3 = "DELETE from selected_question WHERE subject_id = '$id'";
	$result3 = mysqli_query($con, $query3);

	if($result3) {
		$success = "Question paper removed!";
	} else {
		$error = "Error occured! Try again later!";
	}
}

$query4 = "SELECT selected_question.*, subject.code, subject.name as subject FROM selected_question LEFT JOIN subject ON selected_question.subject_id = subject.id";
$result4 = mysqli_query($con, $query4);

$selected_questions = array();
if($result4) {
	while($row = mysqli_fetch_assoc($result4)) {
		$selected_questions[] = $row;
	}
}

$query5 = "SELECT * FROM subject WHERE id NOT IN (SELECT subject_id FROM selected_question)";
$result5 = mysqli_query($con, $query5);

$subjects = array();
if($result5) {
	while($row = mysqli_fetch_assoc($result5)) {
		$subjects[] = $row;
	}
}

$query6 = "SELECT question_set.*, subject.code, subject.name as subject, user.name as created_person FROM question_set LEFT JOIN subject ON (question_set.subject_id = subject.id) LEFT JOIN user ON (question_set.created_by = user.id)";
$result6 = mysqli_query($con, $query6);

$question_sets = array();
if($result6) {
	while($row = mysqli_fetch_assoc($result6)) {
		$question_sets[] = $row;
	}
}

$selected_questions = array();
$query7 = "SELECT selected_question.*, subject.code, subject.name as subject FROM selected_question LEFT JOIN subject on (selected_question.subject_id = subject.id) GROUP BY selected_question.subject_id";
$result7 = mysqli_query($con, $query7);
if($result7) {
	while($row = mysqli_fetch_assoc($result7)) {
		$selected_questions[] = $row;
	}
}

for($i = 0; $i < count($selected_questions); $i++) {
	$subject = $selected_questions[$i]['subject_id'];
	$query8 = "SELECT question_set_id FROM selected_question WHERE subject_id = '$subject'";
	$result8 = mysqli_query($con, $query8);
	if($result8) {
		$question_array = array();
		while($row = mysqli_fetch_assoc($result8)) {
			$question_array[] = $row['question_set_id'];
		}
		$selected_questions[$i]['question_sets'] = $question_array;
		foreach ($question_array as &$value) {
			$value = 'Question Set ' . $value;
		}
		$selected_questions[$i]['question_sets_formatted'] = $question_array;
	}
}

?>

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="page-title">Paper Selection</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="index.php">Home</a>
						</li>
						<li class="breadcrumb-item active">Paper Selection Management</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="card-box mb0">
						<div class="row">
							<div class="col-sm-9"></div>
							<div class="col-sm-3">
								<a href="#add-modal" class="btn btn-default btn-md waves-effect waves-light m-b-30 floatright" data-animation="fadein" data-plugin="custommodal"
								data-overlaySpeed="200" data-overlayColor="#36404a">
								<i class="md md-add"></i> Select Question Set</a>
							</div>
						</div>
						<div class="table-responsive">
							<table id="data" class="table table-hover mails m-0 table table-actions-bar">
								<thead>
									<tr>
										<th>S.No.</th>
										<th>Subject</th>
										<th>Question Sets</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php for ($i = 0; $i < count($selected_questions); $i++) { ?>
									<tr class="data-row" data-subject="<?php echo $selected_questions[$i]['subject_id']; ?>">
										<td>
											<?php echo $i + 1; ?>.
										</td>
										<td>
											<?php echo $selected_questions[$i]['code'] . ' - ' . $selected_questions[$i]['subject']; ?>
										</td>
										<td>
											<?php echo implode(", ", $selected_questions[$i]['question_sets_formatted']); ?>
										</td>
										<td>
											<a href="#delete-modal" class="table-action-btn delete-row" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200"
											data-overlayColor="#36404a">
											<i class="md md-close"></i>
										</a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- container -->
</div>
<!-- content -->
<!--ADD MODAL STARTS HERE-->
<div id="add-modal" class="modal-demo">
	<button type="button" class="close" onclick="Custombox.close();">
		<span>&times;</span>
		<span class="sr-only pointer">Close</span>
	</button>
	<h4 class="custom-modal-title">Select Question Set</h4>
	<div class="custom-modal-text text-left">
		<div class="row">
			<div class="col-md-12">
				<form id="add-form" role="form" method="post">
					<div class="form-group-custom">
						<select id="add-subject" name="add-subject">
							<option data-subject="-1" value="-1">Select Subject</option>
							<?php for($i = 0; $i < count($subjects); $i++) { ?>
							<option value="<?php echo $subjects[$i]['id']; ?>"><?php echo $subjects[$i]['code'] . ' - ' . $subjects[$i]['name']; ?></option>
							<?php } ?>
						</select>
						<label class="control-label">Subject *</label>
						<i class="bar"></i>
					</div>
					<div class="form-group-custom">
						<select id="add-question-set" name="add-question-set[]" multiple>
							<?php for($i = 0; $i < count($question_sets); $i++) { ?>
							<option data-subject="<?php echo $question_sets[$i]['subject_id']; ?>" value="<?php echo $question_sets[$i]['id']; ?>">Question SET <?php echo $question_sets[$i]['id']; ?> - By <?php echo $question_sets[$i]['created_person']; ?></option>
							<?php } ?>
						</select>
						<label class="control-label">Question Set *</label>
						<i class="bar"></i>
					</div>
					<button name="add-data" type="submit" class="ladda-button btn btn-default waves-effect waves-light floatright" data-style="slide-up">Submit</button>
					<button type="button" class="btn btn-danger waves-effect waves-light m-l-10 cancelbtn" onclick="Custombox.close();">Cancel</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!--ADD MODAL ENDS HERE-->
<!--DELETE MODAL STARTS HERE-->
<div id="delete-modal" class="modal-demo">
	<button type="button" class="close" onclick="Custombox.close();">
		<span>&times;</span>
		<span class="sr-only pointer">Close</span>
	</button>
	<h4 class="custom-modal-title">Delete Question Set</h4>
	<div class="custom-modal-text text-left">
		<div class="row">
			<div class="col-md-12">
				<center>
					<img src="assets/images/custom/warning.svg" class="warningicon">
				</center>
				<p class="warningtext">Are you sure you want to delete?</p>
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<form id="delete-form" role="form" method="post">
					<input type="hidden" name="delete-id"/>
					<button name="delete-data" type="submit" class="ladda-button btn btn-default waves-effect waves-light floatright" data-style="slide-up">Confirm</button>
					<button type="button" class="btn btn-danger waves-effect waves-light m-l-10 cancelbtn" onclick="Custombox.close();">Cancel</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!--DELETE MODAL ENDS HERE-->

<script type="text/javascript">
	$(document).ready(function() {
		$('#add-subject').on('change', function() {
			var selected = $(this).val();
			$("#add-question-set option").each(function(e) {
				if ($(this).data("subject") != selected) {
					$(this).hide();
				} else {
					$(this).show();
				}
			});
		});

		$("#add-question-set option").each(function(e) {
			$(this).hide();
		});
	});
	$(document).on("click", ".delete-row", function() {
		var tr = $(this).closest("tr");
		$('#delete-form input[name=delete-id]').val(tr.data("subject"));
	});
</script>

<?php
include_once('footer.php');
?>