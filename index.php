<?php

require_once('common.php');

if ($_SESSION['role_id'] == 1) {
	header('Location: dashboard.php');
} else if ($_SESSION['role_id'] == 2) {
	header('Location: question-set.php');
}  else if ($_SESSION['role_id'] == 3) {
	header('Location: paper-selection.php');
}  else if ($_SESSION['role_id'] == 4) {
	header('Location: question-papers.php');
}   
?>