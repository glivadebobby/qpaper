<?php

require_once('common.php');

$title = "Question Paper";

include_once('header.php');

include_once('sidebar.php');
?>

<?php

$success = "";
$error = "";

$query = "SELECT exam_question.*, subject.code, subject.name as subject, question_set.time_limit FROM exam_question LEFT JOIN subject ON exam_question.subject_id = subject.id LEFT JOIN question ON exam_question.question_id = question.id LEFT JOIN question_set ON question.question_set_id = question_set.id GROUP BY subject_id";
$result = mysqli_query($con, $query);

$selected_questions = array();
if($result) {
	while($row = mysqli_fetch_assoc($result)) {
		$selected_questions[] = $row;
	}
}

?>

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="page-title">Download Paper</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="index.php">Home</a>
						</li>
						<li class="breadcrumb-item active">Download Exam Papers</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="card-box mb0">
						<div class="table-responsive">
							<table id="data" class="table table-hover mails m-0 table table-actions-bar">
								<thead>
									<tr>
										<th>S.No.</th>
										<th>Subject</th>
										<th>Time Limit</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php for ($i = 0; $i < count($selected_questions); $i++) { ?>
									<tr class="data-row" data-subject="<?php echo $selected_questions[$i]['subject_id']; ?>">
										<td>
											<?php echo $i + 1; ?>.
										</td>
										<td>
											<?php echo $selected_questions[$i]['code'] . ' - ' .$selected_questions[$i]['subject']; ?>
										</td>
										<td>
											<?php echo $selected_questions[$i]['time_limit']; ?> mins
										</td>
										<td>
											<form action="download-paper.php" method="post" target="_blank">
												<input type="hidden" name="id" value="<?php echo $selected_questions[$i]['subject_id']; ?>" />
												<button name="download-paper" type="submit" class="ladda-button btn btn-default waves-effect waves-light floatright" data-style="slide-up">Download</button>
											</form>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- container -->
	</div>
	<!-- content -->

	<?php
	include_once('footer.php');
	?>