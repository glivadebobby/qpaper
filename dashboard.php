<?php

require_once('common.php');

$title = "Dashboard";

include_once('header.php');

include_once('sidebar.php');
?>

<?php

$success = "";
$error = "";

$query1 = "SELECT * FROM user";
$result1 = mysqli_query($con, $query1);

$users = mysqli_num_rows($result1);

$query2 = "SELECT * FROM department";
$result2 = mysqli_query($con, $query2);

$departments = mysqli_num_rows($result2);

$query3 = "SELECT * FROM semester";
$result3 = mysqli_query($con, $query3);

$semesters = mysqli_num_rows($result3);

$query4 = "SELECT * FROM subject";
$result4 = mysqli_query($con, $query4);

$subjects = mysqli_num_rows($result4);

?>

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="page-title">Dashboard</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="index.php">Home</a>
						</li>
						<li class="breadcrumb-item active">Dashboard</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6 col-xl-3">
					<div class="mini-stat clearfix card-box">
						<span class="mini-stat-icon bg-info">
							<i class="ti-user text-white"></i>
						</span>
						<div class="mini-stat-info text-right text-dark">
							<span class="counter text-dark" data-plugin="counterup"><?php echo $users; ?></span>
							Users
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-6 col-xl-3">
					<div class="mini-stat clearfix card-box">
						<span class="mini-stat-icon bg-warning">
							<i class="fa fa-building-o text-white"></i>
						</span>
						<div class="mini-stat-info text-right text-dark">
							<span class="counter text-dark" data-plugin="counterup"><?php echo $departments; ?></span>
							Departments
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-6 col-xl-3">
					<div class="mini-stat clearfix card-box">
						<span class="mini-stat-icon bg-success">
							<i class="ti-calendar text-white"></i>
						</span>
						<div class="mini-stat-info text-right text-dark">
							<span class="counter text-dark" data-plugin="counterup"><?php echo $semesters; ?></span>
							Semesters
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-6 col-xl-3">
					<div class="mini-stat clearfix card-box">
						<span class="mini-stat-icon bg-pink">
							<i class="ti-book text-white"></i>
						</span>
						<div class="mini-stat-info text-right text-dark">
							<span class="counter text-dark" data-plugin="counterup"><?php echo $subjects; ?></span>
							Subjects
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- container -->
</div>
<!-- content -->

<?php
include_once('footer.php');
?>