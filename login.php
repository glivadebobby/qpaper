<?php

include('config.php');

$success = "";
$error = "";
if (isset($_POST['submit'])) {
    $mobile = $_POST['username'];
    $password = md5($_POST['password']);
    $query = "SELECT id, role_id, name FROM user WHERE mobile = '$mobile' AND BINARY password = '$password'";
    $result = mysqli_query($con, $query);

    $count = mysqli_num_rows($result);

    if($count == 1) {
        $response = mysqli_fetch_assoc($result);
        session_start();
        $_SESSION['id']= $response['id'];
        $_SESSION['role_id']= $response['role_id'];
        $_SESSION['name']= $response['name'];
        if ($response['role_id'] == 1) {
            header('Location: dashboard.php');
        } else if ($response['role_id'] == 2) {
            header('Location: question-set.php');
        }  else if ($response['role_id'] == 3) {
            header('Location: paper-selection.php');
        }  else if ($response['role_id'] == 4) {
            header('Location: question-paper.php');
        }   
    } else {
        $error = "Unable to login with provided credentials!";
    }
    mysqli_close($con);
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="assets/images/favicon_1.ico">

    <title>Login - QPaper</title>

    <link href="assets/plugins/ladda-buttons/css/ladda-themeless.min.css" rel="stylesheet" type="text/css" />

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="assets/js/modernizr.min.js"></script>

</head>

<body>

    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
        <div class="card-box">
            <div class="panel-heading">
                <h4 class="text-center">Sign In to
                    <strong>QPaper</strong>
                </h4>
            </div>

            <div class="p-20">
                <form method="POST" class="form-horizontal m-t-20">

                    <div class="form-group-custom">
                        <input type="text" name="username" maxlength="10" required/>
                        <label class="control-label" for="username">Username</label>
                        <i class="bar"></i>
                    </div>

                    <div class="form-group-custom">
                        <input type="password" name="password" required/>
                        <label class="control-label" for="password">Password</label>
                        <i class="bar"></i>
                    </div>

                    <div class="form-group text-center m-t-40">
                        <div class="col-12">
                            <button name="submit" class="ladda-button btn btn-success btn-block text-uppercase waves-effect waves-light" type="submit" data-style="slide-up">Login
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <!-- Popper for Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <script src="assets/plugins/ladda-buttons/js/spin.min.js"></script>
        <script src="assets/plugins/ladda-buttons/js/ladda.min.js"></script>
        <script src="assets/plugins/ladda-buttons/js/ladda.jquery.min.js"></script>

        <script src="assets/plugins/parsleyjs/parsley.min.js"></script>

        <script src="assets/plugins/notifyjs/js/notify.js"></script>
        <script src="assets/plugins/notifications/notify-metro.js"></script>

        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('form').parsley();
            });

            Ladda.bind('button[type=submit]');

            <?php
            if ($error != "") {
                echo "$.Notification.notify('error', 'top right', 'Login Failed', '". $error ."');";
            }
            ?>
        </script>
    </body>

    </html>