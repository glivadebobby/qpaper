<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <div id="sidebar-menu">
            <ul>
                <li class="text-muted menu-title">Navigation</li>
                <?php if($_SESSION['role_id'] == 1) { ?>
                <li>
                    <a href="dashboard.php" class="waves-effect">
                        <i class="ti-home"></i>
                        <span> Dashboard </span>
                    </a>
                </li>
                <li>
                    <a href="user.php" class="waves-effect">
                        <i class="ti-user"></i>
                        <span> User Management </span>
                    </a>
                </li>
                <li>
                    <a href="department.php" class="waves-effect">
                        <i class="fa fa-building-o"></i>
                        <span> Departments </span>
                    </a>
                </li>
                <li>
                    <a href="semester.php" class="waves-effect">
                        <i class="ti-calendar"></i>
                        <span> Semesters </span>
                    </a>
                </li>
                <li>
                    <a href="subject.php" class="waves-effect">
                        <i class="ti-book"></i>
                        <span> Subjects </span>
                    </a>
                </li>
                <li>
                    <a href="generate-paper.php" class="waves-effect">
                        <i class="ti-settings"></i>
                        <span> Generate Paper </span>
                    </a>
                </li>
                <?php } ?>
                <?php if($_SESSION['role_id'] == 2) { ?>
                <li>
                    <a href="question-set.php" class="waves-effect">
                        <i class="icon-question"></i>
                        <span> Questions </span>
                    </a>
                </li>
                <?php } ?>
                <?php if($_SESSION['role_id'] == 3) { ?>
                <li>
                    <a href="paper-selection.php" class="waves-effect">
                        <i class="icon-question"></i>
                        <span> Paper Selection </span>
                    </a>
                </li>
                <?php } ?>
                <?php if($_SESSION['role_id'] == 4) { ?>
                <li>
                    <a href="question-paper.php" class="waves-effect">
                        <i class="icon-question"></i>
                        <span> Question Papers </span>
                    </a>
                </li>
                <?php } ?>
                <li>
                    <a href="logout.php" class="waves-effect">
                        <i class="ti-power-off"></i>
                        <span> Logout </span>
                    </a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>