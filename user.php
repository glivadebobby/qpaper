<?php

require_once('common.php');

$title = "User";

include_once('header.php');

include_once('sidebar.php');
?>

<?php

$success = "";
$error = "";

if (isset($_POST['add-data'])) {
	$role = $_POST['add-role'];
	$name = $_POST['add-name'];
	$mobile = $_POST['add-mobile'];
	$email = $_POST['add-email'];
	$password = md5($_POST['add-password']);

	$query1 = "INSERT INTO user(role_id, name, mobile, email, password) VALUES('$role', '$name', '$mobile', '$email', '$password')";
	$result1 = mysqli_query($con, $query1);

	if($result1) {
		$success = "User added!";
	} else {
		$error = "User already exists!";
	}
}

if (isset($_POST['edit-data'])) {
	$id = $_POST['edit-id'];
	$role = $_POST['edit-role'];
	$name = $_POST['edit-name'];
	$mobile = $_POST['edit-mobile'];
	$email = $_POST['edit-email'];

	$query2 = "UPDATE user SET role_id = '$role', name = '$name', mobile = '$mobile', email = '$email' WHERE id = '$id'";
	$result2 = mysqli_query($con, $query2);

	if($result2) {
		$success = "User edited!";
	} else {
		$error = "Error occured! Try again later!";
	}
}

if (isset($_POST['delete-data'])) {
	$id = $_POST['delete-id'];

	$query3 = "DELETE from user WHERE id = '$id'";
	$result3 = mysqli_query($con, $query3);

	if($result3) {
		$success = "User removed!";
	} else {
		$error = "Error occured! Try again later!";
	}
}

$query4 = "SELECT user.id, user.name, user.mobile, user.email, user.role_id, role.role FROM user LEFT JOIN role ON user.role_id = role.id";
$result4 = mysqli_query($con, $query4);

$users = array();
if($result4) {
	while($row = mysqli_fetch_assoc($result4)) {
		$users[] = $row;
	}
}

?>

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="page-title">Users</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="index.php">Home</a>
						</li>
						<li class="breadcrumb-item active">User Management</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="card-box mb0">
						<div class="row">
							<div class="col-sm-9"></div>
							<div class="col-sm-3">
								<a href="#add-modal" class="btn btn-default btn-md waves-effect waves-light m-b-30 floatright" data-animation="fadein" data-plugin="custommodal"
								data-overlaySpeed="200" data-overlayColor="#36404a">
								<i class="md md-add"></i> Add User</a>
							</div>
						</div>
						<div class="table-responsive">
							<table id="data" class="table table-hover mails m-0 table table-actions-bar">
								<thead>
									<tr>
										<th>S.No.</th>
										<th>Role</th>
										<th>Name</th>
										<th>Mobile</th>
										<th>Email ID</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php for ($i = 0; $i < count($users); $i++) { ?>
									<tr class="data-row" data-id="<?php echo $users[$i]['id']; ?>" data-role="<?php echo $users[$i]['role_id']; ?>">
										<td>
											<?php echo $i + 1; ?>.
										</td>
										<td>
											<?php echo $users[$i]['role']; ?>
										</td>
										<td>
											<?php echo $users[$i]['name']; ?>
										</td>
										<td>
											<?php echo $users[$i]['mobile']; ?>
										</td>
										<td>
											<?php echo $users[$i]['email']; ?>
										</td>
										<td>
											<a href="#edit-modal" class="table-action-btn edit-row" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200"
											data-overlayColor="#36404a">
											<i class="md md-edit"></i>
										</a>
										<a href="#delete-modal" class="table-action-btn delete-row" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200"
										data-overlayColor="#36404a">
										<i class="md md-close"></i>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- container -->
</div>
<!-- content -->
<!--ADD MODAL STARTS HERE-->
<div id="add-modal" class="modal-demo">
	<button type="button" class="close" onclick="Custombox.close();">
		<span>&times;</span>
		<span class="sr-only pointer">Close</span>
	</button>
	<h4 class="custom-modal-title">Add User</h4>
	<div class="custom-modal-text text-left">
		<div class="row">
			<div class="col-md-12">
				<form id="add-form" role="form" method="post">
					<div class="form-group-custom">
						<select name="add-role">
							<option value="1">Admin</option>
							<option value="2">Staff</option>
							<option value="3">Scrutinizer</option>
							<option value="4">Institute</option>
						</select>
						<label class="control-label">Role *</label>
						<i class="bar"></i>
					</div>
					<div class="form-group-custom">
						<input name="add-name" type="text" required="required" />
						<label class="control-label">Name *</label>
						<i class="bar"></i>
					</div>
					<div class="form-group-custom">
						<input name="add-mobile" type="text" maxlength="10" required="required" />
						<label class="control-label">Mobile Number *</label>
						<i class="bar"></i>
					</div>
					<div class="form-group-custom">
						<input name="add-email" type="email" required="required" />
						<label class="control-label">EMail ID *</label>
						<i class="bar"></i>
					</div>
					<div class="form-group-custom">
						<input name="add-password" type="text" required="required" />
						<label class="control-label">Password *</label>
						<i class="bar"></i>
					</div>
					<button name="add-data" type="submit" class="ladda-button btn btn-default waves-effect waves-light floatright" data-style="slide-up">Add User</button>
					<button type="button" class="btn btn-danger waves-effect waves-light m-l-10 cancelbtn" onclick="Custombox.close();">Cancel</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!--ADD MODAL ENDS HERE-->
<!--EDIT MODAL STARTS HERE-->
<div id="edit-modal" class="modal-demo">
	<button type="button" class="close" onclick="Custombox.close();">
		<span>&times;</span>
		<span class="sr-only pointer">Close</span>
	</button>
	<h4 class="custom-modal-title">Edit User</h4>
	<div class="custom-modal-text text-left">
		<div class="row">
			<div class="col-md-12">
				<form id="edit-form" role="form" method="post">
					<input type="hidden" name="edit-id"/>
					<div class="form-group-custom">
						<select id="edit-role" name="edit-role">
							<option value="1">Admin</option>
							<option value="2">Staff</option>
							<option value="3">Scrutinizer</option>
							<option value="4">Institute</option>
						</select>
						<label class="control-label">Role *</label>
						<i class="bar"></i>
					</div>
					<div class="form-group-custom">
						<input name="edit-name" type="text" required="required" />
						<label class="control-label">Name *</label>
						<i class="bar"></i>
					</div>
					<div class="form-group-custom">
						<input name="edit-mobile" type="text" maxlength="10" required="required" />
						<label class="control-label">Mobile Number *</label>
						<i class="bar"></i>
					</div>
					<div class="form-group-custom">
						<input name="edit-email" type="text" required="required" />
						<label class="control-label">EMail ID *</label>
						<i class="bar"></i>
					</div>
					<button name="edit-data" type="submit" class="ladda-button btn btn-default waves-effect waves-light floatright" data-style="slide-up">Update User</button>
					<button type="button" class="btn btn-danger waves-effect waves-light m-l-10 cancelbtn" onclick="Custombox.close();">Cancel</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!--EDIT MODAL ENDS HERE-->
<!--DELETE MODAL STARTS HERE-->
<div id="delete-modal" class="modal-demo">
	<button type="button" class="close" onclick="Custombox.close();">
		<span>&times;</span>
		<span class="sr-only pointer">Close</span>
	</button>
	<h4 class="custom-modal-title">Delete User</h4>
	<div class="custom-modal-text text-left">
		<div class="row">
			<div class="col-md-12">
				<center>
					<img src="assets/images/custom/warning.svg" class="warningicon">
				</center>
				<p class="warningtext">Are you sure you want to delete?</p>
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<form id="delete-form" role="form" method="post">
					<input type="hidden" name="delete-id"/>
					<button name="delete-data" type="submit" class="ladda-button btn btn-default waves-effect waves-light floatright" data-style="slide-up">Confirm</button>
					<button type="button" class="btn btn-danger waves-effect waves-light m-l-10 cancelbtn" onclick="Custombox.close();">Cancel</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!--DELETE MODAL ENDS HERE-->

<script type="text/javascript">
	$(document).on("click", ".edit-row", function() {
		var tr = $(this).closest("tr");
		$('#edit-form input[name=edit-id]').val(tr.attr("data-id"));
		$('#edit-role').val(tr.attr("data-role"));
		$('#edit-form input[name=edit-name]').val(
			tr
			.find("td:eq(2)")
			.text()
			.trim()
			);
		$('#edit-form input[name=edit-mobile]').val(
			tr
			.find("td:eq(3)")
			.text()
			.trim()
			);
		$('#edit-form input[name=edit-email]').val(
			tr
			.find("td:eq(4)")
			.text()
			.trim()
			);
	});
	$(document).on("click", ".delete-row", function() {
		var tr = $(this).closest("tr");
		$('#delete-form input[name=delete-id]').val(tr.attr("data-id"));
	});
</script>

<?php
include_once('footer.php');
?>