<?php

require_once('common.php');

$title = "Subject";

include_once('header.php');

include_once('sidebar.php');
?>

<?php

$success = "";
$error = "";

if (isset($_POST['add-data'])) {
	$semester = $_POST['add-semester'];
	$code = $_POST['add-code'];
	$name = $_POST['add-name'];

	$query1 = "INSERT INTO subject(semester_id, code, name) VALUES('$semester', '$code', '$name')";
	$result1 = mysqli_query($con, $query1);

	if($result1) {
		$success = "Subject added!";
	} else {
		$error = "Subject already exists!";
	}
}

if (isset($_POST['edit-data'])) {
	$id = $_POST['edit-id'];
	$semester = $_POST['edit-semester'];
	$code = $_POST['edit-code'];
	$name = $_POST['edit-name'];

	$query2 = "UPDATE subject SET semester_id = '$semester', code = '$code', name = '$name' WHERE id = '$id'";
	$result2 = mysqli_query($con, $query2);

	if($result2) {
		$success = "Subject edited!";
	} else {
		$error = "Error occured! Try again later!";
	}
}

if (isset($_POST['delete-data'])) {
	$id = $_POST['delete-id'];

	$query3 = "DELETE from subject WHERE id = '$id'";
	$result3 = mysqli_query($con, $query3);

	if($result3) {
		$success = "Subject removed!";
	} else {
		$error = "Error occured! Try again later!";
	}
}

$query4 = "SELECT subject.*, semester.name as semester FROM subject LEFT JOIN semester ON subject.semester_id = semester.id";
$result4 = mysqli_query($con, $query4);

$subjects = array();
if($result4) {
	while($row = mysqli_fetch_assoc($result4)) {
		$subjects[] = $row;
	}
}

$query5 = "SELECT semester.*, department.name as department FROM semester LEFT JOIN department ON semester.department_id = department.id";
$result5 = mysqli_query($con, $query5);

$semesters = array();
if($result5) {
	while($row = mysqli_fetch_assoc($result5)) {
		$semesters[] = $row;
	}
}

?>

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="page-title">Subjects</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="index.php">Home</a>
						</li>
						<li class="breadcrumb-item active">Subject Management</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="card-box mb0">
						<div class="row">
							<div class="col-sm-9"></div>
							<div class="col-sm-3">
								<a href="#add-modal" class="btn btn-default btn-md waves-effect waves-light m-b-30 floatright" data-animation="fadein" data-plugin="custommodal"
								data-overlaySpeed="200" data-overlayColor="#36404a">
								<i class="md md-add"></i> Add Subject</a>
							</div>
						</div>
						<div class="table-responsive">
							<table id="data" class="table table-hover mails m-0 table table-actions-bar">
								<thead>
									<tr>
										<th>S.No.</th>
										<th>Semester</th>
										<th>Code</th>
										<th>Subject</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php for ($i = 0; $i < count($subjects); $i++) { ?>
									<tr class="data-row" data-id="<?php echo $subjects[$i]['id']; ?>" data-semester="<?php echo $subjects[$i]['semester_id']; ?>">
										<td>
											<?php echo $i + 1; ?>.
										</td>
										<td>
											<?php echo $subjects[$i]['semester']; ?>
										</td>
										<td>
											<?php echo $subjects[$i]['code']; ?>
										</td>
										<td>
											<?php echo $subjects[$i]['name']; ?>
										</td>
										<td>
											<a href="#edit-modal" class="table-action-btn edit-row" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200"
											data-overlayColor="#36404a">
											<i class="md md-edit"></i>
										</a>
										<a href="#delete-modal" class="table-action-btn delete-row" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200"
										data-overlayColor="#36404a">
										<i class="md md-close"></i>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- container -->
</div>
<!-- content -->
<!--ADD MODAL STARTS HERE-->
<div id="add-modal" class="modal-demo">
	<button type="button" class="close" onclick="Custombox.close();">
		<span>&times;</span>
		<span class="sr-only pointer">Close</span>
	</button>
	<h4 class="custom-modal-title">Add Subject</h4>
	<div class="custom-modal-text text-left">
		<div class="row">
			<div class="col-md-12">
				<form id="add-form" role="form" method="post">
					<div class="form-group-custom">
						<select name="add-semester">
							<?php for($i = 0; $i < count($semesters); $i++) { ?>
							<option value="<?php echo $semesters[$i]['id']; ?>"><?php echo $semesters[$i]['department'] . ' - ' . $semesters[$i]['name']; ?></option>
							<?php } ?>
						</select>
						<label class="control-label">Semester *</label>
						<i class="bar"></i>
					</div>
					<div class="form-group-custom">
						<input name="add-code" type="text" required="required" />
						<label class="control-label">Code *</label>
						<i class="bar"></i>
					</div>
					<div class="form-group-custom">
						<input name="add-name" type="text" required="required" />
						<label class="control-label">Name *</label>
						<i class="bar"></i>
					</div>
					<button name="add-data" type="submit" class="ladda-button btn btn-default waves-effect waves-light floatright" data-style="slide-up">Add Subject</button>
					<button type="button" class="btn btn-danger waves-effect waves-light m-l-10 cancelbtn" onclick="Custombox.close();">Cancel</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!--ADD MODAL ENDS HERE-->
<!--EDIT MODAL STARTS HERE-->
<div id="edit-modal" class="modal-demo">
	<button type="button" class="close" onclick="Custombox.close();">
		<span>&times;</span>
		<span class="sr-only pointer">Close</span>
	</button>
	<h4 class="custom-modal-title">Edit Subject</h4>
	<div class="custom-modal-text text-left">
		<div class="row">
			<div class="col-md-12">
				<form id="edit-form" role="form" method="post">
					<input type="hidden" name="edit-id"/>
					<div class="form-group-custom">
						<select id="edit-semester" name="edit-semester">
							<?php for($i = 0; $i < count($semesters); $i++) { ?>
							<option value="<?php echo $semesters[$i]['id']; ?>"><?php echo $semesters[$i]['department'] . ' - ' . $semesters[$i]['name']; ?></option>
							<?php } ?>
						</select>
						<label class="control-label">Semester *</label>
						<i class="bar"></i>
					</div>
					<div class="form-group-custom">
						<input name="edit-code" type="text" required="required" />
						<label class="control-label">Code *</label>
						<i class="bar"></i>
					</div>
					<div class="form-group-custom">
						<input name="edit-name" type="text" required="required" />
						<label class="control-label">Name *</label>
						<i class="bar"></i>
					</div>
					<button name="edit-data" type="submit" class="ladda-button btn btn-default waves-effect waves-light floatright" data-style="slide-up">Update Subject</button>
					<button type="button" class="btn btn-danger waves-effect waves-light m-l-10 cancelbtn" onclick="Custombox.close();">Cancel</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!--EDIT MODAL ENDS HERE-->
<!--DELETE MODAL STARTS HERE-->
<div id="delete-modal" class="modal-demo">
	<button type="button" class="close" onclick="Custombox.close();">
		<span>&times;</span>
		<span class="sr-only pointer">Close</span>
	</button>
	<h4 class="custom-modal-title">Delete Subject</h4>
	<div class="custom-modal-text text-left">
		<div class="row">
			<div class="col-md-12">
				<center>
					<img src="assets/images/custom/warning.svg" class="warningicon">
				</center>
				<p class="warningtext">Are you sure you want to delete?</p>
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<form id="delete-form" role="form" method="post">
					<input type="hidden" name="delete-id"/>
					<button name="delete-data" type="submit" class="ladda-button btn btn-default waves-effect waves-light floatright" data-style="slide-up">Confirm</button>
					<button type="button" class="btn btn-danger waves-effect waves-light m-l-10 cancelbtn" onclick="Custombox.close();">Cancel</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!--DELETE MODAL ENDS HERE-->

<script type="text/javascript">
	$(document).on("click", ".edit-row", function() {
		var tr = $(this).closest("tr");
		$('#edit-form input[name=edit-id]').val(tr.attr("data-id"));
		$('#edit-semester').val(tr.attr("data-semester"));
		$('#edit-form input[name=edit-code]').val(
			tr
			.find("td:eq(2)")
			.text()
			.trim()
			);
		$('#edit-form input[name=edit-name]').val(
			tr
			.find("td:eq(3)")
			.text()
			.trim()
			);
	});
	$(document).on("click", ".delete-row", function() {
		var tr = $(this).closest("tr");
		$('#delete-form input[name=delete-id]').val(tr.attr("data-id"));
	});
</script>

<?php
include_once('footer.php');
?>