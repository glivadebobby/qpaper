<?php

require_once('common.php');

$title = "Generate Paper";

include_once('header.php');

include_once('sidebar.php');
?>

<?php

$success = "";
$error = "";

if (isset($_POST['generate-paper'])) {
	$id = $_GET['id'];

	$available_query = "SELECT * FROM exam_question WHERE subject_id = '$id'";
	$available_result = mysqli_query($con, $available_query);

	if (mysqli_num_rows($available_result) > 0) {
		$error = "Questions already generated!";
	} else {
		$query1 = "SELECT question.* FROM question LEFT JOIN question_set ON question.question_set_id = question_set.id WHERE question_set.subject_id = '$id' AND mark = 1";
		$result1 = mysqli_query($con, $query1);

		$query2 = "SELECT question.* FROM question LEFT JOIN question_set ON question.question_set_id = question_set.id WHERE question_set.subject_id = '$id' AND mark = 2";
		$result2 = mysqli_query($con, $query2);

		$query3 = "SELECT question.* FROM question LEFT JOIN question_set ON question.question_set_id = question_set.id WHERE question_set.subject_id = '$id' AND mark = 3";
		$result3 = mysqli_query($con, $query3);

		$query4 = "SELECT question.* FROM question LEFT JOIN question_set ON question.question_set_id = question_set.id WHERE question_set.subject_id = '$id' AND mark = 4";
		$result4 = mysqli_query($con, $query4);

		$query5 = "SELECT question.* FROM question LEFT JOIN question_set ON question.question_set_id = question_set.id WHERE question_set.subject_id = '$id' AND mark = 5";
		$result5 = mysqli_query($con, $query5);

		if($result1 && $result2 && $result3 && $result4 && $result5 &&(mysqli_num_rows($result1) > $MAX_MARK_1) && (mysqli_num_rows($result2) > $MAX_MARK_2) && (mysqli_num_rows($result3) > $MAX_MARK_3) && (mysqli_num_rows($result4) > $MAX_MARK_4) && (mysqli_num_rows($result5) > $MAX_MARK_5)) {

			if($result1) {
				$questions = array();
				while($row = mysqli_fetch_assoc($result1)) {
					$questions[] = $row;
				}
				shuffle($questions);
				for($i = 0; $i < 5; $i++) {
					$question_id = $questions[$i]['id'];
					$insert_query = "INSERT INTO exam_question(subject_id, question_id) VALUES('$id', '$question_id')";
					mysqli_query($con, $insert_query);
				}
			}

			if($result2) {
				$questions = array();
				while($row = mysqli_fetch_assoc($result2)) {
					$questions[] = $row;
				}
				shuffle($questions);
				for($i = 0; $i < 5; $i++) {
					$question_id = $questions[$i]['id'];
					$insert_query = "INSERT INTO exam_question(subject_id, question_id) VALUES('$id', '$question_id')";
					mysqli_query($con, $insert_query);
				}
			}

			if($result3) {
				$questions = array();
				while($row = mysqli_fetch_assoc($result3)) {
					$questions[] = $row;
				}
				shuffle($questions);
				for($i = 0; $i < 5; $i++) {
					$question_id = $questions[$i]['id'];
					$insert_query = "INSERT INTO exam_question(subject_id, question_id) VALUES('$id', '$question_id')";
					mysqli_query($con, $insert_query);
				}
			}

			if($result4) {
				$questions = array();
				while($row = mysqli_fetch_assoc($result4)) {
					$questions[] = $row;
				}
				shuffle($questions);
				for($i = 0; $i < 5; $i++) {
					$question_id = $questions[$i]['id'];
					$insert_query = "INSERT INTO exam_question(subject_id, question_id) VALUES('$id', '$question_id')";
					mysqli_query($con, $insert_query);
				}
			}

			if($result5) {
				$questions = array();
				while($row = mysqli_fetch_assoc($result5)) {
					$questions[] = $row;
				}
				shuffle($questions);
				for($i = 0; $i < 5; $i++) {
					$question_id = $questions[$i]['id'];
					$insert_query = "INSERT INTO exam_question(subject_id, question_id) VALUES('$id', '$question_id')";
					mysqli_query($con, $insert_query);
				}
			}
			$success = "Question paper generated!";
		} else {
			$error = "Not enough questions available!";
		}
	}
}

$query6 = "SELECT selected_question.*, subject.code, subject.name as subject, question_set.time_limit, IF(exam_question.id IS NULL, FALSE, TRUE) as generated FROM selected_question LEFT JOIN subject ON selected_question.subject_id = subject.id LEFT JOIN question_set ON selected_question.question_set_id = question_set.id LEFT JOIN exam_question ON selected_question.subject_id = exam_question.subject_id GROUP BY subject_id";
$result6 = mysqli_query($con, $query6);

$selected_questions = array();
if($result6) {
	while($row = mysqli_fetch_assoc($result6)) {
		$selected_questions[] = $row;
	}
}

?>

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="page-title">Generate Paper</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="index.php">Home</a>
						</li>
						<li class="breadcrumb-item active">Generate Exam Papers</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="card-box mb0">
						<div class="table-responsive">
							<table id="data" class="table table-hover mails m-0 table table-actions-bar">
								<thead>
									<tr>
										<th>S.No.</th>
										<th>Subject</th>
										<th>Time Limit</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php for ($i = 0; $i < count($selected_questions); $i++) { ?>
									<tr class="data-row" data-subject="<?php echo $selected_questions[$i]['subject_id']; ?>">
										<td>
											<?php echo $i + 1; ?>.
										</td>
										<td>
											<?php echo $selected_questions[$i]['code'] . ' - ' .$selected_questions[$i]['subject']; ?>
										</td>
										<td>
											<?php echo $selected_questions[$i]['time_limit']; ?> mins
										</td>
										<td>
											<?php if ($selected_questions[$i]['generated']) { ?>
											<span class="label label-success">Generated</span>
											<?php } else { ?>
											<form action="generate-paper.php?id=<?php echo $selected_questions[$i]['subject_id']; ?>" method="post">
												<button name="generate-paper" type="submit" class="btn btn-default waves-effect waves-light floatright">Generate</button>
											</form>
											<?php } ?>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- container -->
	</div>
	<!-- content -->

	<?php
	include_once('footer.php');
	?>