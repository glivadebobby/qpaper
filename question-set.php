<?php

require_once('common.php');

$title = "Question Sets";

include_once('header.php');

include_once('sidebar.php');
?>

<?php

$success = "";
$error = "";

if (isset($_POST['add-data'])) {
	$subject = $_POST['add-subject'];
	$time = $_POST['add-time'];
	$created_by = $_SESSION['id'];

	$query1 = "INSERT INTO question_set(subject_id, time_limit, created_by) VALUES('$subject', '$time', '$created_by')";
	$result1 = mysqli_query($con, $query1);

	if($result1) {
		$success = "Question set added!";
	} else {
		$error = "Question set already exists!";
	}
}

if (isset($_POST['edit-data'])) {
	$id = $_POST['edit-id'];
	$subject = $_POST['edit-subject'];
	$time = $_POST['edit-time'];

	$query2 = "UPDATE question_set SET subject_id = '$subject', time_limit = '$time' WHERE id = '$id'";
	$result2 = mysqli_query($con, $query2);

	if($result2) {
		$success = "Question set edited!";
	} else {
		$error = "Error occured! Try again later!";
	}
}

if (isset($_POST['delete-data'])) {
	$id = $_POST['delete-id'];

	$query3 = "DELETE from question_set WHERE id = '$id'";
	$result3 = mysqli_query($con, $query3);

	if($result3) {
		$success = "Question set removed!";
	} else {
		$error = "Error occured! Try again later!";
	}
}

$created_by = $_SESSION['id'];
$query4 = "SELECT question_set.*, subject.code, subject.name as subject FROM question_set LEFT JOIN subject ON question_set.subject_id = subject.id WHERE created_by = '$created_by'";
$result4 = mysqli_query($con, $query4);

$question_sets = array();
if($result4) {
	while($row = mysqli_fetch_assoc($result4)) {
		$question_sets[] = $row;
	}
}

$query5 = "SELECT * FROM subject";
$result5 = mysqli_query($con, $query5);

$subjects = array();
if($result5) {
	while($row = mysqli_fetch_assoc($result5)) {
		$subjects[] = $row;
	}
}

?>

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<h4 class="page-title">Question Sets</h4>
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="index.php">Home</a>
						</li>
						<li class="breadcrumb-item active">Question Set Management</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="card-box mb0">
						<div class="row">
							<div class="col-sm-9"></div>
							<div class="col-sm-3">
								<a href="#add-modal" class="btn btn-default btn-md waves-effect waves-light m-b-30 floatright" data-animation="fadein" data-plugin="custommodal"
								data-overlaySpeed="200" data-overlayColor="#36404a">
								<i class="md md-add"></i> Add Question Set</a>
							</div>
						</div>
						<div class="table-responsive">
							<table id="data" class="table table-hover mails m-0 table table-actions-bar">
								<thead>
									<tr>
										<th>S.No.</th>
										<th>Subject</th>
										<th>Time Limit</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php for ($i = 0; $i < count($question_sets); $i++) { ?>
									<tr class="data-row" data-id="<?php echo $question_sets[$i]['id']; ?>" data-subject="<?php echo $question_sets[$i]['subject_id']; ?>" data-time="<?php echo $question_sets[$i]['time_limit']; ?>">
										<td>
											<a href="question.php?id=<?php echo $question_sets[$i]['id']; ?>"><?php echo $i + 1; ?>. <i class="md md-info"></i></a>
										</td>
										<td>
											<?php echo $question_sets[$i]['subject']; ?>
										</td>
										<td>
											<?php echo $question_sets[$i]['time_limit']; ?> mins
										</td>
										<td>
											<a href="#edit-modal" class="table-action-btn edit-row" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200"
											data-overlayColor="#36404a">
											<i class="md md-edit"></i>
										</a>
										<a href="#delete-modal" class="table-action-btn delete-row" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200"
										data-overlayColor="#36404a">
										<i class="md md-close"></i>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- container -->
</div>
<!-- content -->
<!--ADD STARTS HERE-->
<div id="add-modal" class="modal-demo">
	<button type="button" class="close" onclick="Custombox.close();">
		<span>&times;</span>
		<span class="sr-only pointer">Close</span>
	</button>
	<h4 class="custom-modal-title">Add Question Set</h4>
	<div class="custom-modal-text text-left">
		<div class="row">
			<div class="col-md-12">
				<form id="add-form" role="form" method="post">
					<div class="form-group-custom">
						<select name="add-subject">
							<?php for($i = 0; $i < count($subjects); $i++) { ?>
							<option value="<?php echo $subjects[$i]['id']; ?>"><?php echo $subjects[$i]['code'] . ' - ' . $subjects[$i]['name']; ?></option>
							<?php } ?>
						</select>
						<label class="control-label">Subject *</label>
						<i class="bar"></i>
					</div>
					<div class="form-group-custom">
						<input name="add-time" type="number" required="required" />
						<label class="control-label">Time Limit *</label>
						<i class="bar"></i>
					</div>
					<button name="add-data" type="submit" class="ladda-button btn btn-default waves-effect waves-light floatright" data-style="slide-up">Add Question Set</button>
					<button type="button" class="btn btn-danger waves-effect waves-light m-l-10 cancelbtn" onclick="Custombox.close();">Cancel</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!--ADD MODAL ENDS HERE-->
<!--EDIT MODAL STARTS HERE-->
<div id="edit-modal" class="modal-demo">
	<button type="button" class="close" onclick="Custombox.close();">
		<span>&times;</span>
		<span class="sr-only pointer">Close</span>
	</button>
	<h4 class="custom-modal-title">Edit Question Set</h4>
	<div class="custom-modal-text text-left">
		<div class="row">
			<div class="col-md-12">
				<form id="edit-form" role="form" method="post">
					<input type="hidden" name="edit-id"/>
					<div class="form-group-custom">
						<select id="edit-subject" name="edit-subject">
							<?php for($i = 0; $i < count($subjects); $i++) { ?>
							<option value="<?php echo $subjects[$i]['id']; ?>"><?php echo $subjects[$i]['code'] . ' - ' . $subjects[$i]['name']; ?></option>
							<?php } ?>
						</select>
						<label class="control-label">Subject *</label>
						<i class="bar"></i>
					</div>
					<div class="form-group-custom">
						<input name="edit-time" type="text" required="required" />
						<label class="control-label">Time Limit *</label>
						<i class="bar"></i>
					</div>
					<button name="edit-data" type="submit" class="ladda-button btn btn-default waves-effect waves-light floatright" data-style="slide-up">Update Question Set</button>
					<button type="button" class="btn btn-danger waves-effect waves-light m-l-10 cancelbtn" onclick="Custombox.close();">Cancel</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!--EDIT MODAL ENDS HERE-->
<!--DELETE MODAL STARTS HERE-->
<div id="delete-modal" class="modal-demo">
	<button type="button" class="close" onclick="Custombox.close();">
		<span>&times;</span>
		<span class="sr-only pointer">Close</span>
	</button>
	<h4 class="custom-modal-title">Delete Question Set</h4>
	<div class="custom-modal-text text-left">
		<div class="row">
			<div class="col-md-12">
				<center>
					<img src="assets/images/custom/warning.svg" class="warningicon">
				</center>
				<p class="warningtext">Are you sure you want to delete?</p>
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<form id="delete-form" role="form" method="post">
					<input type="hidden" name="delete-id"/>
					<button name="delete-data" type="submit" class="ladda-button btn btn-default waves-effect waves-light floatright" data-style="slide-up">Confirm</button>
					<button type="button" class="btn btn-danger waves-effect waves-light m-l-10 cancelbtn" onclick="Custombox.close();">Cancel</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!--DELETE MODAL ENDS HERE-->

<script type="text/javascript">
	$(document).on("click", ".edit-row", function() {
		var tr = $(this).closest("tr");
		$('#edit-form input[name=edit-id]').val(tr.attr("data-id"));
		$('#edit-subject').val(tr.attr("data-subject"));
		$('#edit-form input[name=edit-time]').val(tr.attr("data-time"));
	});
	$(document).on("click", ".delete-row", function() {
		var tr = $(this).closest("tr");
		$('#delete-form input[name=delete-id]').val(tr.attr("data-id"));
	});
</script>

<?php
include_once('footer.php');
?>